const modalMessage = {
  title: "Remover",
  description: "¿Esta seguro que quiere eliminar?",
};

export default modalMessage;
