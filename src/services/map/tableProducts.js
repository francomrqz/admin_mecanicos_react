export const tableProducts = {
  cols: [
    { name: "code", label: "Código" },
    { name: "name", label: "Nombre" },
    { name: "category", label: "Categoria" },
    { name: "brand", label: "Marca" },
    { name: "price", label: "Precio" }
  ]
}
export const tableCategory = {
  cols: [
    { name: "code", label: "Código" },
    { name: "name", label: "Nombre" },
  ]
}
export const tableBrand = {
  cols: [
    { name: "code", label: "Código" },
    { name: "name", label: "Nombre" },
  ]
}

