export const formProductsView = [
  {
    link: "products/product",
    type: "product",
    label: "Producto",
    fields: [],
    visible: false,
  },
  {
    link: "products/category",
    type: "category",
    label: " Categoria",
    fields: [],
    visible: true,
  },
  {
    link: "products/brands",
    type: "brands",
    label: "Marca",
    fields: [],
    visible: true,
  },
];
