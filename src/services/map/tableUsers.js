export const tableUsers = {
  cols: [
    { name: "givenName", label: "Nombre" },
    { name: "familyName", label: "Apellido" },
    { name: "email", label: "Email" },
    { name: "type", label: "Tipo" },
  ]
}

