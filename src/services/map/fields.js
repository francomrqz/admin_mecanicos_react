export const fieldsProduct = [
  {
    name: 'Nombre',
    nameInput: 'name',
    placeholder: '',
    isUpdate: false,
    isUpdateStatus: false,
    value: undefined,
    type: 'text',
    required: false
  }, {
    name: 'Categoria',
    nameInput: 'category',
    placeholder: '',
    isUpdate: true,
    isUpdateStatus: false,
    value: undefined,
    type: 'select',
    options: [],
    required: false
  }, {
    name: 'Marca',
    nameInput: 'brand',
    placeholder: 'Ej: Samsung',
    isUpdate: true,
    isUpdateStatus: false,
    value: undefined,
    type: 'select',
    options: [],
    required: false
  }, {
    name: 'Precio',
    nameInput: 'price',
    placeholder: '',
    isUpdate: false,
    isUpdateStatus: false,
    value: undefined,
    type: 'number',
    required: false
  }, {
    name: 'Código',
    nameInput: 'code',
    value: undefined,
    placeholder: '',
    isUpdate: false,
    isUpdateStatus: false,
    type: 'text',
    required: false
  }
];

export const fieldCategory = [{
  name: 'Nueva categoria',
  nameInput: 'category',
  placeholder: '',
  isUpdate: false,
  isUpdateStatus: false,
  value: undefined,
  type: 'text',
  required: false
}]

export const fieldBrand = [{
  name: 'Nueva marca',
  nameInput: 'brand',
  placeholder: '',
  isUpdate: false,
  isUpdateStatus: false,
  value: undefined,
  type: 'text',
  required: false
}]