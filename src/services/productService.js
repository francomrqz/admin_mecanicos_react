import axios from './api';
import { environment } from '../environments/environment';

export const saveProduct = async (newProduct) => {
  return await axios.post(environment.apiUrl + 'products', newProduct)
}

/* export const uploadProduct = async (fileList) => {
  if (fileList.length > 0) {
    let file: File = fileList[0];
    let formData: FormData = new FormData();
    let params = new HttpParams();
    formData.append('products-excel-file', file);
    const options = {
      params: params,
      reportProgress: true,
    };
    return await axios.post(environment.apiUrl + this.tableName + '/excel/upload', formData, options).toPromise();
  }
} */

export const getProductFiltered = async (query) => {
  return await axios.get(environment.apiUrl + 'products/filter?' + query) /* brand='+idBrand */
}

export const getDbExcel = async () => {
  return await axios.get(environment.apiUrl + 'products/excel/download')
}

export const uploadFileProduct = async (fileList) => {

  if (fileList.length > 0) {
    let file = fileList[0];
    let formData = new FormData();
    formData.append('products-excel-file', file);
    const options = {
      reportProgress: true,
    }
    return await axios.post(environment.apiUrl + 'products/excel/upload', formData, options);
  }
}

export const createProdct = async (values) => {
  console.log(values);
}

export const getProducts = async () => {
  return await axios.get(environment.apiUrl + 'products')
}

export const removeProduct = async (id) => {
  return await axios.post(environment.apiUrl + 'products/delete', { id: id })
}

