import axios from './api';
import { environment } from '../environments/environment';
const tableName = 'brands'

export const saveBrand = async (brandName) => {
  return await axios.post(environment.apiUrl + 'brands', { name: brandName })
}

export const getBrands = async () => {
  return await axios.get(environment.apiUrl + tableName)
}

export const getBrandsById = async (id) => {
  return await axios.get(environment.apiUrl + tableName + '/' + id)
}

export const removeBrand = async (id) => {
  return await axios.post(environment.apiUrl + tableName + '/remove', { id })
}

export const getBrandsSolid = async () => {
  return await axios.get(environment.apiUrl + 'brandsproviders')
}

export const getProdcutsImages = async (brand) => {
  return await axios.get(environment.apiUrl + 'assets/images/' + brand)
}

export const uploadFile = async (file, fileType) => {
  let methodFile = ''
  switch (fileType) {
    case 'brands':
      methodFile = 'brandsproviders'
      break;
    case 'family':
      methodFile = 'familyproviders'
      break;
    case 'products':
      methodFile = 'products'
      break;
    default:
      methodFile = 'brandsproviders'
      break;
  }

  return await axios.post(environment.apiUrl + methodFile + '/uploadFile', file)
}

export const findImage = (image) => {
  let xhr = new XMLHttpRequest();
  xhr.open('HEAD', image, false);
  xhr.send();
  return xhr.status === 404 ? false : true;
}
