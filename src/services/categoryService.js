import axios from './api';
import { environment } from '../environments/environment';

export const saveCategory = async (categoryName) => {
  return await axios.post(environment.apiUrl + 'category', { name: categoryName })
}

export const getCategories = async () => {
  return await axios.get(environment.apiUrl + 'category')
}

export const getCategoryById = async (id) => {
  return await axios.get(environment.apiUrl + 'category/' + id)
}

export const removeCategory = async (id) => {
  return await axios.post(environment.apiUrl + 'category/remove', { id })
}
