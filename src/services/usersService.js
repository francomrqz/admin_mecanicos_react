import axios from './api';
import { environment } from '../environments/environment';

export const sendDataUser = async (values) => {
  try {
    return await axios.post(environment.apiUrl + 'users/create', values);
  } catch (error) {
    console.log(error);
  }
}

export const getUser = async () => {
  return await axios.get(environment.apiUrl + 'users');
}
export const removeUser = async (idUser) => {
  return await axios.post(environment.apiUrl + 'user/remove', { idUser })
}