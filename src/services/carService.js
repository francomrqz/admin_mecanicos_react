import axios from './api';
import { environment } from '../environments/environment';


export const getBrands = async () => {
  try {
    return await axios.get(environment.apiUrl + 'brands')
  } catch (e) {
    return [];
  }
}

export const getBrandsSolid = async () => {
  try {
    return await axios.get(environment.apiUrl + 'brandsproviders')
  } catch (e) {
    return [];
  }
}

export const setBrand = async () => {
  try {
    return await axios.post(environment.apiUrl + 'brands', newBrand)
  } catch (e) {
    return [];
  }
}

export const removeBrand = async (idBrand) => {
  try {
    return await axios.get(environment.apiUrl + 'brands/remove', { brand: idBrand})
  } catch (e) {
    return [];
  }
}

export const getFamilySolid = async (idBrand) => {
  try {
    return await axios.get(environment.apiUrl + 'familyproviders')
  } catch (e) {
    return [];
  }
}

export const uploadFile = async (file, fileType) => {
  try {
    let methodFile = ''
    switch (fileType) {
        case 'brands':
            methodFile = 'brandsproviders'
            break;
        case 'family':
            methodFile = 'familyproviders'
            break;
        case 'products':
            methodFile = 'products'
            break;
    }
    
    return axios.post(environment.apiUrl + methodFile +'/uploadFile', file)
  } catch (e) {
    return [];
  }
}

export const findImage = async () => {
  let xhr = new XMLHttpRequest();
  xhr.open('HEAD', image, false);
  xhr.send();
  return xhr.status == 404 ? false :  true;
}

export const getProdcutsImages = async (brand) => {
  try {
    return await axios.get(environment.apiUrl + 'assets/images/'+brand).toPromise()
  } catch (e) {
    return [];
  }
}