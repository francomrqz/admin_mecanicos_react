import axios from './api';
import { environment } from '../environments/environment';

export const setUser = async (newValue) => {
  localStorage.setItem('user', JSON.stringify(newValue))
}

export const getUser = async () => {
  return await localStorage.getItem('user')
}

export const findUser = async (email) => {
  try {
    return await axios.get(environment.apiUrl + 'usersadmin?email=' + email)
  } catch (error) {
    return []
  }
}

export const getUsers = async () => {
  try {
    return await axios.get(environment.apiUrl + 'users')
  } catch (error) {
    return []
  }
}

export const signOut = async () => {
  localStorage.removeItem('user')
  this.authService.signOut();
  this.router.navigate(['/']);
}

export const getSession = () => {
  const session = localStorage.getItem('user');
  return session ? session : null;
}

export const doLogout = () => {
  localStorage.removeItem('user');
  localStorage.removeItem('token');
};