export const fieldsSellers = [
  {
    name: 'Nombre',
    nameInput: 'givenName',
    isUpdate: false,
    isUpdateStatus: false,
    placeholder: 'Ingresar Nombre',
    type: 'text',
    required: false,
    value: ""
  },
  {
    name: 'Apellido',
    nameInput: 'familyName',
    isUpdate: false,
    isUpdateStatus: false,
    placeholder: 'Ingresar Apellido',
    type: 'text',
    required: false,
    value: ""
  },
  {
    name: 'Email',
    nameInput: 'email',
    isUpdate: false,
    isUpdateStatus: false,
    placeholder: 'Ingresar Email',
    type: 'email',
    required: false,
    value: ""
  },
  {
    name: 'Contraseña',
    nameInput: 'password',
    isUpdate: false,
    isUpdateStatus: false,
    placeholder: 'Ingresar Contraseña',
    type: 'password',
    required: false,
    value: ""
  },
  {
    name: 'Tipo',
    nameInput: 'select',
    isUpdate: true,
    isUpdateStatus: false,
    placeholder: '',
    type: 'select',
    options: [
      {
        id: 0,
        name: 'vendedor'
      },
      {
        id: 1,
        name: 'empleado'
      },
      {
        id: 2,
        name: 'administrador'
      }
    ],
    value: "",
    required: false
  }
];
