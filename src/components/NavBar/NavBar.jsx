import React from 'react';
import PropTypes from 'prop-types';
import { NavLink, Link } from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import styles from './NavBar.module.scss';

const NavBar = ({ onDoLogout, session, bg, variant }) => {

  return (
    <Navbar bg={bg} variant={variant} expand="md" className={`px-5 ${styles.NavBar}`} fixed="top">
      <Navbar.Brand as={Link} to="/">Admin MEC</Navbar.Brand>
      <Navbar.Toggle aria-controls="main-nav" />
      <Navbar.Collapse id="main-nav">
        <Nav className="ml-auto">
          <div className="row mr-2">
            <Nav.Item  >
              <Nav.Link activeClassName='active' as={NavLink} to="/products" >Productos</Nav.Link>
            </Nav.Item>

            <Nav.Item>
              <Nav.Link activeClassName='active' as={NavLink} to="/users">Usuarios</Nav.Link>
            </Nav.Item>

            <Nav.Item>
              <Nav.Link activeClassName='active' as={NavLink} to="/aplication">Aplicacion</Nav.Link>
            </Nav.Item>

            <Nav.Item>
              <Nav.Link as={NavLink} to="/profile" className="mr-4">Mi perfil</Nav.Link>
            </Nav.Item>

          </div>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}


NavBar.propTypes = {
  bg: PropTypes.oneOf([
    'primary',
    'secondary',
    'success',
    'info',
    'warning',
    'danger',
    'light',
    'dark',
    'transparent'
  ]),
  variant: PropTypes.oneOf([
    'dark',
    'light'
  ]),
  session: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  onDoLogout: PropTypes.func
};

NavBar.defaultProps = {
  bg: 'transparent',
  variant: 'light',
  session: null,
  onDoLogout: () => { }
};

export default NavBar;