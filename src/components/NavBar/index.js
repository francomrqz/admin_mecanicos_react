import { connect } from "react-redux";
import store from "../../store";
import { doLogout } from "../../services/loginService";
import NavBar from "./NavBar";

const mapStateToProps = (state) => {
  return {
    session: state.session,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onDoLogout: async () => {
    doLogout();
    //dispatch(modifySession(null));
  },
  getStatus: () => {
    /* store.dispatch({ type: 'MODIFY_SESSION' }) */
    store.subscribe(() => {
      console.log(store.getState());
    });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
