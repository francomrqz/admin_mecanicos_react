import React from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import { BsFillTrashFill } from 'react-icons/bs';

const TablePrimary = ({ titleCols, valueCols, openWarningModal }) => {

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          {titleCols.map((item, index) => (
            <th key={index} >{item.label}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {
          valueCols.map((item, index) => (
            <tr key={index}>
              {item.map((el, i) => (
                <td key={i} className={i === 0 ? "hide" : ""}>{el}</td>
              ))}
              <td><Button variant="warning" size="sm" onClick={() => openWarningModal(item)}> <BsFillTrashFill /> </Button></td>
            </tr>
          ))}
      </tbody>
    </Table>
  )
}

export default TablePrimary;