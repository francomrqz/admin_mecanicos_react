import { connect } from 'react-redux';
import FormAdd from './FormAdd';

const mapStateToProps = (state) => ({
  session: state.session,
});

const mapDispatchToProps = (dispatch, fields, visible, sendForm) => ({
  getFields:  () => {
    return fields
  }
});
export default connect(mapStateToProps,mapDispatchToProps)(FormAdd);
