import React, { useState, useEffect, useRef } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Spinner from 'react-bootstrap/Spinner';

const FormAdd = ({ fields, sendForm, formDefault, isLoadingSend = false }) => {

  const [dataForm, addDataToForm] = useState({});
  const [isVisible, showForm] = useState(formDefault.visible);
  const [formSelected, setForm] = useState(formDefault.typeForm)
  const formRef = useRef(null);

  useEffect(() => {
    if (formDefault.typeForm !== formSelected) {
      setForm(formDefault)
      formRef.current.reset();
    }
    if (fields.length) {
      let stateInitForm = {}
      fields.forEach(element => {
        let valueDefault = ""
        if (element.type === "select") {
          valueDefault = element.options.length ? element.options[0].id : null
        }
        stateInitForm[element.nameInput] = valueDefault
      });
      addDataToForm(stateInitForm)
    }
    // eslint-disable-next-line
  }, [fields]);

  const handChange = (field, nameInput) => {
    const valueInput = field.target.value;
    addDataToForm({ ...dataForm, [nameInput]: valueInput })
  }

  const showField = (field) => {
    if (field.type === 'select' && field.options.length > 0) {
      return <Form.Control as="select"
        onChange={(e) => { handChange(e, field.nameInput) }}
        name={field.nameInput}>
        {field.options.map((option, index) =>
          <option
            key={index}
            value={option.id}>
            {option.name}
          </option>
        )}
      </Form.Control>
    } else {
      return <Form.Control name={field.nameInput}
        onChange={(e) => { handChange(e, field.nameInput) }}
        type={field.type}
        refs="name"
        value={dataForm[field.nameInput] || ''}
        placeholder={field.placeholder} />
    }
  }

  const renderForm = (formDefault, fields) => {
    return <Card className={isVisible ? 'visible' : 'hide'}>
      <Card.Body>

        <Form ref={formRef} >
          <Row>
            {fields.map((field, index) =>
              <Col key={index} className="mt-4" xs={{ span: 12 }} md={{ span: 4 }}>
                <Form.Group controlId={index}>
                  <Form.Label>{field.name}</Form.Label>
                  {showField(field)}
                </Form.Group>
              </Col>
            )}
          </Row>
          <div className="d-flex justify-content-end">
            <Button variant="warning" className="mr-2" onClick={() => showForm(false)}>Cancelar</Button>
            <Button variant="primary" type="button" onClick={() => sendForm(dataForm)} className="float-right">
              {isLoadingSend ? <Spinner animation="grow" size="sm" className="mr-2" as="span" variant="info"  > </Spinner> : ""}
              Agregar
            </Button>
          </div>
        </Form>
      </Card.Body>
    </Card>
  }

  return (
    renderForm(formDefault, fields)
  )
}

export default FormAdd;