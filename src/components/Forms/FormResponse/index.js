import { connect } from 'react-redux';
import FormResponse from './FormResponse';

const mapStateToProps = (state) => ({
  session: state.session,
});

const mapDispatchToProps = (dispatch, response) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(FormResponse);
