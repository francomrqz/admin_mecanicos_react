import React from 'react';
import Alert from 'react-bootstrap/Alert';
/* import styles from './FormAdd.module.scss' */

const FormResponse = ({ response }) => {

  return (<Alert className={response.visible ? "visible" : "hide"} variant={response.color}>
    {response.message}
  </Alert>
  )

}

export default FormResponse;