import { connect } from 'react-redux';
import RemoveModal from './RemoveModal';

const mapStateToProps = (state) => ({
  session: state.session
})

export default connect(mapStateToProps)(RemoveModal);