import React, {useState} from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import modalMessage from "../../utils/modal.message"

const RemoveModal = ({ show, onHideModal, data, description = modalMessage.description, title =  modalMessage.title, removeItem }) => {

  const remove = () => {
    onHideModal(false)
    removeItem(data)
  }

  return (
    <div>
      <Modal show={show} onHide={() => onHideModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{title} {data.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{description} </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => onHideModal(false)}>
            Cancelar
          </Button>
          <Button variant="danger" onClick={() => remove()}>
            Borrar
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

export default RemoveModal