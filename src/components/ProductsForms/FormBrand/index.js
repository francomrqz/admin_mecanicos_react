
import { getBrands, saveBrand } from '../../../services/brandService';
import FormBrand from "./FormBrand"
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
  return {
    products: state.products
  }
};

const mapDispatchToProps = (dispatch) => ({
  getBrands: async () => {
    return await getBrands()
  },
  saveBrandName: async (brandData) => {
    console.log(brandData);
    return await saveBrand(brandData.brand)
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(FormBrand);
