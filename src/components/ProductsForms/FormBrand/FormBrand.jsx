import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import FormAdd from '../../Forms/FormAdd';
import { formProductsView } from '../../../services/map/forms';
import { fieldBrand } from '../../../services/map/fields';
import React from 'react';

const FormBrand = ({ showDiferentForm, sendForm }) => {
  return (
    <Row>
      <Col>
        <FormAdd fields={fieldBrand}
          closeForm={(form) => showDiferentForm(form, false)}
          formDefault={formProductsView[2]}
          sendForm={(valuesInput) => sendForm(valuesInput)}
        >
        </FormAdd>
      </Col>
    </Row>
  )
}

export default FormBrand