import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import FormAdd from '../../Forms/FormAdd';
import { formProductsView } from '../../../services/map/forms';
import { fieldsProduct } from '../../../services/map/fields';
import React, { useEffect } from 'react';

const FormProduct = ({ showDiferentForm, saveProduct, categoriesOptions, brandsOptions }) => {


  useEffect(() => {
    const optionsCategories = fieldsProduct.find(el => el.nameInput === "category");
    const optionsBrands = fieldsProduct.find(el => el.nameInput === "brand");
    optionsCategories.options = categoriesOptions;
    optionsBrands.options = brandsOptions;
    // eslint-disable-next-line
  }, []);

  return (
    <Row>
      <Col>
        <FormAdd fields={fieldsProduct}
          closeForm={(form) => showDiferentForm(form, false)}
          formDefault={formProductsView[0]}
          sendForm={(valuesInput) => saveProduct(valuesInput)}
        >
        </FormAdd>
      </Col>
    </Row>
  )
}

export default FormProduct