
import FormProduct from "./FormProduct"
import { saveProduct } from '../../../services/productService';

import { connect } from 'react-redux';

const mapStateToProps = (state) => {
  return {
    products: state.products
  }
};

const mapDispatchToProps = (dispatch) => ({
  saveProduct: async (productData) => {
    await saveProduct(productData);
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(FormProduct);
