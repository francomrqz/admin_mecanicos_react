
import { saveCategory, getCategories } from '../../../services/categoryService';
import FormCategory from "./FormCategory"
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
  return {
    products: state.products
  }
};

const mapDispatchToProps = (dispatch) => ({
  getCategories: async () => {
    return await getCategories()
  },
  saveCategoryName: async (categoryName) => {
    console.log(categoryName);
    return await saveCategory(categoryName)
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(FormCategory);
