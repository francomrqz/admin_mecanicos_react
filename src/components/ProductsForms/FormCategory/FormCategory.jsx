import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import FormAdd from '../../Forms/FormAdd';
import { formProductsView } from '../../../services/map/forms';
import { fieldCategory } from '../../../services/map/fields';
import React from 'react';

const FormCategory = ({ showDiferentForm, saveCategoryName }) => {
  return (
    <Row>
      <Col>
        <FormAdd fields={fieldCategory}
          closeForm={(form) => showDiferentForm(form, false)}
          formDefault={formProductsView[1]}
          sendForm={(valuesInput) => saveCategoryName(valuesInput)}
        >
        </FormAdd>
      </Col>
    </Row>
  )
}

export default FormCategory