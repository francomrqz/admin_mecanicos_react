import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Dropdown from 'react-bootstrap/Dropdown';
import RemoveModal from '../RemoveModal'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { BsFillPlusCircleFill, BsTrashFill } from 'react-icons/bs';

const MenuTable = ({ uploadFile, getBrands, getProductFilteredByBrand, renderProducts, showForm, forms, removeBrand }) => {

  const [brands, setBrands] = useState([]);
  const [brand, setBrand] = useState([]);
  const [linkActive, setLinkActive] = useState("product");
  const [isVisibleModal, showModal] = useState(false);

  useEffect(() => {
    getListBrands()
    // eslint-disable-next-line 
  }, []);

  const removeModalData = {
    title: "Remover",
    description: "¿Esta seguro que quiere elimintarlo?",
  }

  const getListBrands = async () => {
    const { data } = await getBrands();
    if (data.length) {
      setBrands(data);
      setBrand(data[0])
      getProductsByBrand(data[0])
    }
  }

  const getProductsByBrand = async (brand) => {
    const query = 'brand=' + brand.id
    const productsByBrand = await getProductFilteredByBrand(query, 'brand')
    setBrand(brand);
    renderProducts(productsByBrand);
  }

  const uploadExcel = () => {
    const fileUpload = document.getElementById('fileUpload')
    fileUpload.click();
    fileUpload.onchange = async (inputValue: any) => {
      uploadFile(inputValue.target.files)
    }
  }

  const removeItem = (id) => {
    removeBrand(id)
    getListBrands()
  }

  const dropDown = () => {
    if (brands.length && linkActive === 'product') {
      return <Dropdown className="mx-2 float-left">
        <Dropdown.Toggle variant="success" id="dropdown-basic">
          Marcas {brand.name}
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {brands.map((item, index) => (
            <div key={index.toString()}>
              <Dropdown.Item onClick={() => { getProductsByBrand(item) }} >{item.name}</Dropdown.Item>
            </div>
          ))}
        </Dropdown.Menu>
      </Dropdown>
    }
  }

  const showView = (item) => {
    setLinkActive(item.typeForm)
    showForm(item)
  }

  return (
    <div>
      <RemoveModal
        show={isVisibleModal}
        showModal={showModal}
        removeItem={(data) => removeItem(data.id)}
        data={brand}
        title={removeModalData.title}
        description={removeModalData.description} />
      <Row className="justify-content-center align-items-center">
        <Col lg="8">
          {dropDown()}
          <input id="fileUpload" type="file" className="hide" />
          <Button variant="secondary" onClick={() => uploadExcel()} size="md" className=" mx-2 float-left" >
            Subir lista de productos
          </Button>

          {forms.map((item, key) => (
            <Button key={key} variant="light"
              onClick={() => showView(item)}
              size="md"
              className={`mx-2 float-left ${item.visible ? 'active' : 'inactive'}`} >

              <BsFillPlusCircleFill className="pb-1" />{item.label} {item.visible}
            </Button>

          ))}
        </Col>
        <Col lg="4" className="text-right">
          <Button variant="primary" size="md" className="my-4 mx-2" >Descargar Base </Button>
          {(brands.length > 0) ?
            <Button variant="warning" onClick={() => showModal(true)} size="md" className="my-4 mx-2" > <BsTrashFill />  {brands[0].name} </Button>
            : ""}
        </Col>
      </Row>
    </div>
  )
};

export default MenuTable;