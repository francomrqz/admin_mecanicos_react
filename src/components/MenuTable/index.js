import { connect } from 'react-redux';
import MenuTable from './MenuTable';
import { getBrands, removeBrand } from './../../services/brandService';
import { getProductFiltered, uploadFileProduct } from './../../services/productService';

const mapStateToProps = (state) => ({
  session: state.session,
  products: state.products,
});

const mapDispatchToProps = (dispatch) => ({
  getBrands: async () => {
    return await getBrands()
  },
  getProductFilteredByBrand: async (query, type) => {
    const { data } = await getProductFiltered(query)
    return data.products
  },
  uploadFile: async (file) => {
    return await uploadFileProduct(file)
  },
  removeBrand: async (idBrand) => {
    return await removeBrand(idBrand)
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(MenuTable);
