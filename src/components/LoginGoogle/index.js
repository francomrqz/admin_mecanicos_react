import { connect } from 'react-redux';
import { findUser, setUser } from '../../services/loginService';
import LoginGoogle from './LoginGoogle';
import store from '../../store'

const mapStateToProps = (state) => ({
  session: state.session,
});

const mapDispatchToProps = (dispatch) => ({
  findUser: async (user, err) => {
    if (!err) {
      if (user) {
        console.log(user);
        const resp = await findUser(user._profile.email);
        if (resp) {
          //store.dispatch({ type: 'MODIFY_SESSION' , session: user._profile})
          setUser(user._profile)
        }
      }
    }
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginGoogle);
