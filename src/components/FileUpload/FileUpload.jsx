import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Media from 'react-bootstrap/Media';
import styles from './FileUpload.module.scss';
import Button from 'react-bootstrap/Button';


const FileUpload = ({loadFile}) => {
  return (
    <div>
      <Button variant="primary" size="lg" onClick={()=> loadFile}>Subir Lista de Precios </Button>
      <input type="file" id="fileUpload" name="fileUpload" style="display:none;"/>
    </div>
  )
};



export default FileUpload;