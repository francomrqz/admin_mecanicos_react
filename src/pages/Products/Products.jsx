import React, { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import styles from './Products.module.scss';
import { formProductsView } from '../../services/map/forms';
//   components
import MenuTable from '../../components/MenuTable';
import RemoveModal from '../../components/RemoveModal/RemoveModal'
import BrandView from './BrandView'
import CategoryView from './CategoryView'
import ProductView from './ProductView'

const Products = ({ mapProductsTable, removeItem }) => {

  const [formDefault, showForm] = useState(formProductsView[0]);
  const [isLoadingActive, showLoadingForm] = useState(false)
  const [isRefreshTable, refreshTable] = useState(false)
  const [elementToRemove, AddElementToRemove] = useState({ id: null });
  const [isVisibleModal, showModalRemove] = useState(false);

  const showDiferentForm = async (formSelected) => {

    formProductsView.forEach((e) => {
      e.visible = false;
      if (formSelected.type === e.type) {
        formSelected.visible = true
      }
    });

    showForm(formSelected)
  }

  const openModalRemove = (item) => {
    AddElementToRemove({ id: item[0] })
    showModalRemove(true)
  }

  const getOptionsToSelect = (options) => {
    return options.map((item, index) => [item.id, item.name]);
  }

  const removeElement = (id) => {
    removeItem(formDefault.typeForm, id)
    refreshTable(!isRefreshTable)
  }

  return (
    <Container fluid className={styles.Products}>
      <RemoveModal
        show={isVisibleModal}
        onHideModal={(hide) => showModalRemove(false)}
        removeItem={(data) => removeElement(data.id)}
        data={elementToRemove}/>
      <Row className="justify-content-md-center">
        <Col xs lg="2">
          {isLoadingActive ? <Spinner animation="grow" size="sm" className="mr-2" as="span" variant="info"  > </Spinner> : ""}
        </Col>
      </Row>
      <Row>
        <Col>
          <MenuTable
            renderProducts={(productsByBrand) => mapProductsTable(productsByBrand)}
            showForm={(form) => showDiferentForm(form)}
            forms={formProductsView}></MenuTable>
        </Col>
         { formDefault.type === "product" ?
            <ProductView openWarningModal={(product)=>openModalRemove(product)} />
          : formDefault.type === "category" ?
            <CategoryView openWarningModal={(category)=>openModalRemove(category)} />
          :
            <BrandView openWarningModal={(category)=>openModalRemove(category)} />
          }
      </Row>
    </Container>
  )
}

export default Products;