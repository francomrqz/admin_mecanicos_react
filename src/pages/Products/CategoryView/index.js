import { saveCategory, getCategories } from "../../../services/categoryService";
import CategoryView from "./CategoryView";
import { connect } from "react-redux";

const mapStateToProps = (state) => {
  return {
    categories: state.categories,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getCategories,
  saveCategoryName: async (categoryName) => {
    console.log(categoryName);
    return await saveCategory(categoryName);
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryView);
