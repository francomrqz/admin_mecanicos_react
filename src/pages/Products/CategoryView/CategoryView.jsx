import Col from 'react-bootstrap/Col';
import FormAdd from '../../../components/Forms/FormAdd';
import { formProductsView } from '../../../services/map/forms';
import { tableCategory } from '../../../services/map/tableProducts';
import { fieldCategory } from '../../../services/map/fields';
import TablePrimary from '../../../components/Tables/TablePrimary';
import React, {useEffect, useState } from 'react';


const CategoryView = ({getCategories, openWarningModal, saveCategoryName }) => {
  
  const [tableCategoryData, addCategories] = useState([])
  useEffect(() => {
    getCategoriesFromService() 
  },[])

  const getCategoriesFromService = async () => {
    const res = await getCategories();
    const mapOfCategories = res.data.map((item) => [
      item.id,
      item.name
    ])
    addCategories(mapOfCategories);
  }
  return (
    <>
      <Col xs={{ span: 12, order: 2 }} md={{ span: 12, order: 1 }}>
        <FormAdd fields={fieldCategory}
          formDefault={formProductsView[1]}
          sendForm={(valuesInput) => saveCategoryName(valuesInput.category)}
        >
        </FormAdd>
      </Col>

      <Col xs={{ span: 12, order: 2 }} md={{ span: 12, order: 1 }}>
        <TablePrimary
          titleCols={tableCategory.cols}
          valueCols={tableCategoryData}
          openWarningModal={(element) => { openWarningModal(element) }} />
      </Col>
    </>
  )
}

export default CategoryView