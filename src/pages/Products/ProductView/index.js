import ProductView from "./ProductView";
import { saveProduct } from "../../../services/productService";
import { connect } from "react-redux";
import { getProducts } from "../../../services/productService";
const mapStateToProps = (state) => {
  return {
    products: state.products,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getProducts,
  saveProduct,
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductView);
