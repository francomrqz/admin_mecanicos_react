import Col from 'react-bootstrap/Col';
import FormAdd from '../../../components/Forms/FormAdd';
import { formProductsView } from '../../../services/map/forms';
import { fieldsProduct } from '../../../services/map/fields';
import { tableProducts } from '../../../services/map/tableProducts';
import React,{useEffect, useState} from 'react';
import TablePrimary from '../../../components/Tables/TablePrimary';

const ProductView = ({getProducts, saveProduct, openWarningModal }) => {


  const [tableProductData, addProducts] = useState([])
  useEffect(() => {
    getProductsFromService()
  },[])

  const getProductsFromService = async () => {
    const res = await getProducts();
    addProducts(res.data)
  }

  return (
    <>
      <Col xs={{ span: 12, order: 2 }} md={{ span: 12, order: 1 }}>
        <FormAdd fields={fieldsProduct}
          formDefault={formProductsView[0]}
          sendForm={(data) => saveProduct(data.product)}
        />
      </Col>

      <Col xs={{ span: 12, order: 2 }} md={{ span: 12, order: 1 }}>
        <TablePrimary
          titleCols={tableProducts.cols}
          valueCols={tableProductData}
          openWarningModal={()=>openWarningModal(true) } />
      </Col>
    </>
  )
}
export default ProductView