import { connect } from "react-redux";
import Products from "./Products";
import { modifyProducts } from "../../store/actions";
import {
  getCategoryById,
  removeCategory,
} from "../../services/categoryService";
import { getBrandsById, removeBrand } from "../../services/brandService";
import { removeProduct } from "../../services/productService";

const mapStateToProps = (state) => {
  return {
    products: state.products,
    categories: state.categories,
    brands: state.brands,
  };
};

const mapDispatchToProps = (dispatch) => ({
  removeItem: async (typeForm, id) => {
    if (typeForm === "brand") {
      await removeBrand(id);
    } else if (typeForm === "category") {
      await removeCategory(id);
    } else {
      await removeProduct(id);
    }
  },

  mapProductsTable: async (list) => {
    let productsTable = [];
    if (list.length) {
      const brandId = list[0].brand || null;
      const categoryId = list[0].category || null;
      const branData = brandId ? await getBrandsById(brandId) : null;
      const categoryData = categoryId
        ? await getCategoryById(categoryId)
        : null;
      const brandName = branData ? branData.data.name : null;
      const categoryName = categoryData ? categoryData.data.name : null;

      productsTable = list.map((item, index) => [
        index,
        item.id,
        item.name,
        categoryName,
        brandName,
        item.price,
      ]);
      dispatch({ type: "PRODUCTS", products: productsTable });
    }
  },
  dispatchProducts: (resMap) => {
    dispatch(modifyProducts(resMap));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);
