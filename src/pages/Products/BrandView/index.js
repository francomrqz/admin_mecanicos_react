import { getBrands, saveBrand } from "../../../services/brandService";
import BrandView from "./BrandView";
import { connect } from "react-redux";

const mapStateToProps = (state) => {
  return {
    brands: state.brands,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getBrands,
  saveBrandName: async (brandData) => {
    console.log(brandData);
    return await saveBrand(brandData.brand);
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(BrandView);
