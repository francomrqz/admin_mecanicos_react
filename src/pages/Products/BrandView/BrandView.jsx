import Col from 'react-bootstrap/Col';
import FormAdd from '../../../components/Forms/FormAdd';
import { formProductsView } from '../../../services/map/forms';
import { tableBrand } from '../../../services/map/tableProducts';
import { fieldBrand } from '../../../services/map/fields';
import TablePrimary from '../../../components/Tables/TablePrimary'
import React, {useEffect, useState } from 'react';

const BrandView = ({getBrands, openWarningModal, saveBrandName }) => {

  const [tableBrandsData, addBrands] = useState([])
  useEffect(() => {
    getBrandsFromService() 
  },[])

  const getBrandsFromService = async () => {
    const res = await getBrands();
    const mapOfBrands = res.data.map((item) => [
      item.id,
      item.name
    ])
    addBrands(mapOfBrands);
  }
  
  return (
    <>
      <Col xs={{ span: 12, order: 2 }} md={{ span: 12, order: 1 }}>
        <FormAdd fields={fieldBrand}
          formDefault={formProductsView[2]}
          sendForm={(valuesInput) => saveBrandName(valuesInput.brand)}
        >
        </FormAdd>
      </Col>

      <Col xs={{ span: 12, order: 2 }} md={{ span: 12, order: 1 }}>
        <TablePrimary
          titleCols={tableBrand.cols}
          valueCols={tableBrandsData}
          openWarningModal={(element) => { openWarningModal(element) }} />
      </Col>
    </>
  )
}

export default BrandView