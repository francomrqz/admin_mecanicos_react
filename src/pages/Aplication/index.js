import { connect } from 'react-redux';
import Aplication from './Aplication';

const mapStateToProps = (state) => ({
  session: state.session,
});

export default connect(mapStateToProps)(Aplication);
