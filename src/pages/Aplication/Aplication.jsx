import React from 'react';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import styles from './Aplication.module.scss';

const Aplication = ({ session }) => (
  <Container fluid className={styles.Aplication}>
    <Row className={styles.row}>
      <Col xs={{ span: 12, order: 2 }} md={{ span: 6, order: 1 }} className={styles.col}>
        <div className="w-100">

        </div>
      </Col>
    </Row>
  </Container>
);

Aplication.propTypes = {
  session: PropTypes.oneOfType([ PropTypes.object, PropTypes.bool ]),
};

Aplication.defaultProps = {
  session: null,
};

export default Aplication;