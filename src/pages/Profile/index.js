import { connect } from 'react-redux';
import Profile from './Profile';

const mapStateToProps = (state) => ({
  session: state.session,
});

export default connect(mapStateToProps)(Profile);
