import { connect } from 'react-redux';
import Home from './Home';
import { getProductFiltered } from './../../services/productService'
import { getUsers } from './../../services/loginService'
const mapStateToProps = (state) => ({
  session: state.session,
});

const mapDispatchToProps = () => ({
  findProduct(query){
    return getProductFiltered(query)
  },
  providers: async () => {
    const users = await getUsers()
    console.log(users);
    const provider = users.data.filter(item => item.type === 'provider')
    const cantProvider = provider.length
    const cantMechanic = users.data.length - provider.length
    const percentageProvider = (provider.length * 100) / users.data.length
    const percentageMechanic = 100 - percentageProvider
    const user = { provider, cantProvider, cantMechanic, percentageProvider, percentageMechanic }
    console.log(user);
    return user

  }
})



export default connect(mapStateToProps, mapDispatchToProps)(Home);
