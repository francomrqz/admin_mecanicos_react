import React from 'react';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import styles from './Home.module.scss';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

const Home = ({ cantProvider, percentageProvider, cantMechanic, percentageMechanic }) => {

  const options = {
    chart: {
      plotBorderWidth: null,
      plotShadow: false
    },
    title: {
      text: 'Usuarios registrados (Repuesteros y Mecánicos)'
    },
    tooltip: {
      pointFormat: '{series.name}: <b> {point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false
        },
        showInLegend: true
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
        {
          name: 'Provedores:  ' + cantProvider,
          y: percentageProvider
        }, {
          name: 'Mecanicos: ' + cantMechanic,
          y: percentageMechanic
        }
      ]
    }]
  }

  return (
    <Container fluid className={styles.Home}>
      <Row className={styles.row}>
        <Col xs={{ span: 12, order: 2 }} md={{ span: 12, order: 1 }} className={styles.col}>
          <HighchartsReact
            highcharts={Highcharts}
            options={options} />
        </Col>
      </Row>
    </Container>
  );
}

Home.propTypes = {
  session: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

Home.defaultProps = {
  session: null,
};

export default Home;