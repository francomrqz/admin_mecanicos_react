import React, { useState, useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import FormAdd from '../../components/Forms/FormAdd';
import { tableUsers } from "../../services/map/tableUsers";
import TablePrimary from '../../components/Tables/TablePrimary'
import styles from './Users.module.scss';
import { fieldsSellers } from '../../services/mock/sellers';
import FormResponse from '../../components/Forms/FormResponse';
import RemoveModal from '../../components/RemoveModal/RemoveModal'
import Fade from "react-bootstrap/Fade";

const UsersView = ({ session, users, getUsers, createUser, removeUser }) => {

  const [formDefault, showForm] = useState({
    typeForm: "",
    label: "",
    visible: false,
    fields: fieldsSellers
  });

  const [isFadeResponse, showFadeRessponse] = useState(false);
  const [isVisibleModal, showModalRemove] = useState(false);
  const [elementSelected, AddElementToRemove] = useState({ id: null });

  const removeModalData = {
    title: "Remover",
    description: "¿Esta seguro que quiere eliminar?",
  }

  const [isVisibleForm, showFormAdd] = useState(false);
  const [responseForm, setResponseForm] = useState({
    color: "",
    visible: false,
    message: ""
  });
  const [tableData, setTableData] = useState({
    cols: [],
    data: []
  })

  const sendForm = async (data) => {
    const resp = await createUser(data)
    getUsersList()
    setResponseForm({ ...resp, visible: true })
    showNotification()
  }

  const showDiferentForm = () => {
    formDefault.visible = true;
    showForm(formDefault)
    showFormAdd(true)
  }

  const openModalRemove = (elem) => {
    AddElementToRemove({ id: elem[0], name: elem[1] })
    showModalRemove(true)
  }

  useEffect(() => {
    getUsersList()
    // eslint-disable-next-line 
  }, []);

  const getUsersList = async () => {
    const users = await getUsers();
    const userList = users.map((item, index) => {
      return [item.id,
      item.givenName,
      item.familyName,
      item.email,
      item.type
      ]
    })
    renderTable(tableUsers.cols, userList)
  }

  const renderTable = (cols, data) => {
    setTableData({
      cols, data
    })
  }

  const removeUserData = async (data) => {
    await removeUser(data.id)
    const message = {
      visible: true, color: "primary", message: `usuario ${data.name} removido`
    }
    setResponseForm(message)
    showNotification()
  }

  const showNotification = () => {
    showFadeRessponse(true)
    setTimeout(() => {
      showFadeRessponse(false)
    }, 3000);
  }


  return (
    <div>
      <Container fluid className={styles.Users} >
        <Row className={styles.row}>
          <Col xs={{ span: 12 }} md={{ span: 12 }} className="my-2" >
            <Button variant="primary" onClick={() => { showDiferentForm() }}>Agregar</Button>
          </Col>

          <Col xs={{ span: 12 }} md={{ span: 12 }} className={styles.col, isVisibleForm ? 'visible' : 'hide'} >
            <FormAdd
              fields={formDefault.fields}
              closeForm={(form) => showFormAdd(false)}
              sendForm={(param) => sendForm(param)}
              formDefault={formDefault}
            />
          </Col>

          <Col xs={{ span: 12 }} md={{ span: 12 }} className="mt-4" >

            <div className={isFadeResponse ? "visible" : "hide"}>
              <Fade>
                <FormResponse response={responseForm}> </FormResponse>
              </Fade>
            </div>
            <TablePrimary
              titleCols={tableData.cols}
              valueCols={tableData.data}
              openModalRemove={(element) => { openModalRemove(element) }} />
          </Col>
        </Row>
        <RemoveModal
          show={isVisibleModal}
          showModal={showModalRemove}
          removeItem={(data) => removeUserData(data)}
          data={elementSelected}
          title={removeModalData.title}
          description={removeModalData.description} />
      </Container>
    </div >
  );
}

export default UsersView;