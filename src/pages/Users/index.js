import { connect } from 'react-redux';
import Users from './Users';
import { modifyUsers } from '../../store/actions';
import { getUser, sendDataUser, removeUser } from '../../services/usersService';
const mapStateToProps = (state) => ({
  session: state.session,
  users: state.users,
});

const mapDispatchToProps = (dispatch) => ({
  getUsers: async () => {
    const { data } = await getUser();
    return data
  },

  dipatchUsers: (users) => {
    dispatch(modifyUsers(users))
  },
  createUser: async (infoUser) => {
    const { data } = await sendDataUser(infoUser);
    console.log(data);
    return data
  },
  removeUser: async (idUser) => {
    console.log(idUser);
    removeUser(idUser)
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Users);
