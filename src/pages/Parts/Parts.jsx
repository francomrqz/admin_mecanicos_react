import React from 'react';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import styles from './Parts.module.scss';

const Parts = (props) => {
  
    return (
      <Container fluid className={styles.Parts}>
        <Row className={styles.row}>
          <Col xs={{ span: 12, order: 2 }} md={{ span: 6, order: 1 }} className={styles.col}>
            <div className="w-100">
              <Row>
                <Col lg={{ span: 4, offset: 2 }}>
                  <Button variant="secondary" block as={Link} to="/movies" className="mb-2">Movie List</Button>
                </Col>
                {!this.props.session && <Col lg={4}>
                  <Button variant="outline-primary" block as={Link} to="/login" className="mb-2">Login</Button>
                </Col>}
              </Row>
            </div>
          </Col>
          <Col xs={{ span: 12, order: 1 }} md={{ span: 6, order: 2 }} className={styles.col}>
            <img src="assets/cinema.svg" alt="cinema" className="w-75" />
          </Col>
        </Row>
      </Container>
    );
}

Parts.propTypes = {
  session: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

Parts.defaultProps = {
  session: null,
};

export default Parts;