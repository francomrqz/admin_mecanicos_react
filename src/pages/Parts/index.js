import { connect } from 'react-redux';
import Parts from './Parts';

const mapStateToProps = (state) => ({
  session: state.session,
});

export default connect(mapStateToProps)(Parts);
