import { connect } from 'react-redux';
import Login from './Login';
const mapStateToProps = (state) => ({
  session: state.session,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchLogin() {
    dispatch({ type: 'MODIFY_SESSION', session: true });
  }
});


export default connect(mapStateToProps, mapDispatchToProps)(Login);
