import { connect } from 'react-redux';
import Frame from './Frame';

const mapStateToProps = (state) => ({
  session: state.session,
});

export default connect(mapStateToProps)(Frame);
