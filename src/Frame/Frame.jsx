import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter, Switch } from 'react-router-dom';
import NavBar from '../components/NavBar';

const Frame = (props) => {
  return (
    <BrowserRouter>
      {props.session ? <NavBar bg="primary" variant="dark" /> : ''}
      <div className='mt-80'>

        <Switch>
          {props.children}
        </Switch>
      </div>
    </BrowserRouter>
  )
}


Frame.propTypes = {
  children: PropTypes.node,
  session: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

Frame.defaultProps = {
  children: (<span />),
  session: null
};

export default Frame;