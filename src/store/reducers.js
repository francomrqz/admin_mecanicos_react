import { MODIFY_SESSION, PRODUCTS, CATEGORIES, BRANDS, USERS } from "./actions";

const initialState = {
  session: true,
  products: [],
  categories: [],
  brands: [],
};

const addItem = (state, action) => {
  switch (action.type) {
    case PRODUCTS:
      return {
        ...state,
        products: action.products,
      };
    case MODIFY_SESSION:
      return {
        ...state,
        session: action.session,
      };
    case MODIFY_SESSION:
      return {
        ...state,
        session: action.session,
      };
    case USERS:
      return {
        ...state,
        users: action.users,
      };
    case CATEGORIES:
      return {
        ...state,
        categories: action.categories,
      };
    case BRANDS:
      return {
        ...state,
        brands: action.brands,
      };
    default:
      return state;
  }
};

const reducers = (state = initialState, action) => ({
  ...addItem(state, action),
});

export default reducers;
