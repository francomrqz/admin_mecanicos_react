export const USERS = 'USERS';
export const MODIFY_SESSION = 'MODIFY_SESSION';
export const PRODUCTS = 'PRODUCTS';
export const BRANDS = 'BRANDS';
export const CATEGORIES = 'CATEGORIES';

export const modifySession = (session) => ({
  type: MODIFY_SESSION,
  session
});

export const modifyProducts = (products) => ({
  type: PRODUCTS,
  products
});

export const modifyUsers = (users) => ({
  type: USERS,
  users
});

export const listBrands = (brands) => ({
  type: BRANDS,
  brands
});
export const listCategories = (categories) => ({
  type: CATEGORIES,
  categories
});

