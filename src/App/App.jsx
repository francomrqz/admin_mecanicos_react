import React from "react";
import { Provider } from 'react-redux'
import { Route } from "react-router-dom";
import store from '../store';
import Frame from '../Frame';
import Products from '../pages/Products'
import BrandView from '../pages/Products/BrandView'
import CategoryView from '../pages/Products/CategoryView'
import Aplication from '../pages/Aplication'
import Users from '../pages/Users'
import profile from '../pages/Profile'

import Login from "../pages/Login";

const App = () => {

  return (
    <Provider store={store}>
      <Frame>
        <Route exact path="/" component={Login} />
        <Route exact path="/products" component={Products} />
        <Route exact path="/products/brands" component={BrandView} />
        <Route exact path="/products/category" component={CategoryView} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/users" component={Users} />
        <Route exact path="/profile" component={profile} />
        <Route exact path="/aplication" component={Aplication} />
      </Frame>
    </Provider>
  )
}

export default App;